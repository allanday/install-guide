:BOOKID: install-guide

:BZURL: link:https://pagure.io/fedora-docs/install-guide/issues[https://pagure.io/]

:HOLDER: Red Hat, Inc. and others

:NEXTVER: 35

:PREVVER: 33

:PRODUCT: Fedora

:PRODVER: Rawhide

:YEAR: 2021
