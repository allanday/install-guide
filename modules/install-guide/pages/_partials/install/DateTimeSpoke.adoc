
:experimental:

[[sect-installation-gui-date-and-time]]
=== Date & Time

The `Date & Time` screen allows you to configure time and date-related settings for your system. This screen is automatically configured based on the settings you selected in xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-welcome[Welcome Screen and Language Selection], but you can change your date, time and location settings before you begin the installation.

.Date & Time

image::anaconda/DateTimeSpoke.png[Screenshot of the Date & Time screen, showing a map in the center, region and city selection on top, and manual time settings at the bottom.]

First, select your `Region` using the drop-down menu in the top left corner of the screen. Then, select your `City`, or the city closest to your location in the same time zone. Selecting a specific location helps {PRODUCT} ensure that your time is always set correctly including automatic time changes for daylight savings time if applicable.

You can also select a time zone relative to Greenwich Mean Time (GMT) without setting your location to a specific region. To do so, select `Etc` as your region.

[NOTE]
====

The list of cities and regions comes from the Time Zone Database ([package]*tzdata*) public domain, which is maintained by the Internet Assigned Numbers Authority (IANA). The Fedora Project cannot add cities or regions into this database. You can find more information at the link:++https://www.iana.org//time-zones++[IANA official website].

====

The switch labeled `Network Time` in the top right corner of the screen can be used to enable or disable network time synchronization using the Network Time Protocol (NTP). Enabling this option will keep your system time correct as long as the system can access the internet. By default, four NTP _pools_ are configured; you can add others and disable or remove the default ones by clicking the gear wheel button next to the switch.

.The Add and mark for usage NTP servers dialog

image::anaconda/DateTimeSpoke_AddNTP.png[A dialog window allowing you to add or remove NTP pools from your system configuration, check their status and mark them for use.]

If you disable network time synchronization, the controls at the bottom of the screen will become active, and you will be able to set the current time and date manually.

After configuring your time and date settings, press the `Done` button in the top left corner to return to xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-installation-summary[Installation Summary].
