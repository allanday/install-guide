
:experimental:

[[sect-installation-gui-manual-partitioning-swraid]]
==== Creating Software RAID

_Redundant arrays of independent disks_ (RAIDs) are constructed from multiple storage devices that are arranged to provide increased performance and, in some configurations, greater fault tolerance. See xref:Installing_Using_Anaconda.adoc#sect-installation-gui-manual-partitioning-filesystems[Device, File System and RAID Types] for a description of different kinds of RAIDs.

A RAID device is created in one step, and disks are added or removed as necessary. One RAID partition per physical disk is allowed for each device, so the number of disks available to the installation program determines which levels of RAID device are available to you. For example, if your system has two hard drives, the installation program will not allow you to create a RAID10 device, which requires 4 separate partitions.

[IMPORTANT]
====

This section only explains how to create software RAID with standard (physical) partitions. However, you can also configure LVM volume groups and Btrfs volumes to use RAID and place their logical volumes or Btrfs subvolumes on top of this RAID array. See xref:Installing_Using_Anaconda.adoc#sect-installation-gui-manual-partitioning-lvm[Creating a Logical Volume Management (LVM) Layout] and xref:Installing_Using_Anaconda.adoc#sect-installation-gui-manual-partitioning-btrfs[Creating a Btrfs Layout] for instructions on creating RAID in LVM and Btrfs.

====

.Create Software RAID

image::anaconda/CustomSpoke_SoftwareRAID.png[The Manual Partitioning screen, showing available options for a selected standard partition on software RAID. The RAID Level menu is open, showing all available RAID levels.]

[NOTE]
====

RAID configuration options are only visible if you have selected two or more disks for installation. At least two disks are required to create a RAID device, and some RAID layouts will require more. Requirements for different types of RAID are described in xref:Installing_Using_Anaconda.adoc#sect-installation-gui-manual-partitioning-filesystems[Device, File System and RAID Types].

====

Follow the procedure below to create software RAID:

.Creating Software RAID
. Click the `+` button at the bottom of the list showing existing mount points. A new dialog window will open.

. In the new dialog window, specify a mount point for which you want to create a separate software RAID partition - for example, `/`. Optionally, specify a size for the new partition using standard units such as MB or GB (for example, `50GB`). Then, click `Add mount point` to add the mount point and return to the main partitioning screen.
+
[NOTE]
====

When creating a mount point for swap on software RAID, specify the mount point as `swap`.

====

. The mount point has now been created using the default settings, which means it has been created as an LVM logical volume. Select the newly created mount point in the left pane to configure it further, and convert it to a software RAID partition by changing the `Device Type` option to `RAID`.

. Choose a RAID type from the `RAID Level` drop-down menu. Available RAID types and their requirements are described in xref:Installing_Using_Anaconda.adoc#sect-installation-gui-manual-partitioning-filesystems[Device, File System and RAID Types].

. In the `Device(s)` section on the right side of the screen, you can see that the partition has been assigned to several physical disks. Click the `Modify` button to configure on which drives this partition will be created.

. In the `Configure Mount Point` dialog, you can specify which physical devices (disks) this partition *may* reside on. You can select one or more disks which will be used to hold this partition by holding down kbd:[Ctrl] and clicking each disk in the list. If you want to make sure that this partition is placed on a specific set of hard drives, select only those drives and unselect all others.
+
After you finish configuring the partition's location, click `Save` to return to the main `Manual Partitioning` screen.

. Configure other settings specific to the partition - its `Mount Point`, `Desired Capacity`, and `File System`. Press `Update Settings` to apply any changes to the configuration.

Repeat this procedure for any additional standard partitions with software RAID you want to create.
