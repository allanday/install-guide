
:experimental:

[[sect-installation-gui-manual-partitioning-standard]]
==== Creating Standard Partitions

_Standard partitions_ are the most common type of partition, with the widest support across operating systems. For example, Microsoft Windows uses exclusively physical partitions and cannot natively work with LVM or Btrfs. Most {PRODUCT} partitioning setups will also require at least one standard partition for the `/boot` directory, and possibly also another standard partition with the BIOS Boot or EFI System file system to store the boot loader.

See xref:appendixes/Disk_Partitions.adoc#appe-disk-partitions-overview[An Introduction to Disk Partitions] for additional information about the concepts behind physical partitions.

.Create Standard Partition

image::anaconda/CustomSpoke_AddPhysical.png[The Manual Partitioning screen, showing available options for a selected standard partition.]

Follow the procedure below to create mount points on standard physical partitions:

.Creating Standard Partitions
. Click the `+` button at the bottom of the list showing existing mount points. A new dialog window will open.

. In the new dialog window, specify a mount point for which you want to create a separate mount point - for example, `/`. Optionally, specify a size for the partition using standard units such as MB or GB (for example, `50GB`). Then, click `Add mount point` to add the mount point and return to the main partitioning screen.
+
[NOTE]
====

When creating a swap partition, specify the mount point as `swap`. For a BIOS Boot partition, use `biosboot`. For an EFI System Partition, use `/boot/efi`.

For information about these partition types, see xref:Installing_Using_Anaconda.adoc#sect-installation-gui-manual-partitioning-recommended[Recommended Partitioning Scheme].

====

. The mount point has now been created using the default settings, which means it has been created as an LVM logical volume. Select the newly created mount point in the left pane to configure it further, and convert it to a physical partition by changing the `Device Type` option to `Standard Partition`. Then, click `Update Settings` in the bottom right corner of the screen.

. In the `Device(s)` section on the right side of the screen, you can see that the partition has been assigned to one or more hard drives. Click the `Modify` button to configure on which drive this partition will be created.

. In the `Configure Mount Point` dialog, you can specify which physical devices (disks) this volume *may* reside on. You can select one or more disks which will be used to hold this volume by holding down kbd:[Ctrl] and clicking each disk in the list. If you select multiple disks here, [application]*Anaconda* will determine where exactly the partition should be created based on how you configured the rest of the installation. If you want to make sure that this partition is placed on a specific hard drive, select only that drive and unselect all others.
+
After you finish configuring the partition's location, click `Save` to return to the main `Manual Partitioning` screen.

. Configure other settings specific to the partition - its `Mount Point`, `Desired Capacity`, and `File System`. Press `Update Settings` to apply any changes to the configuration.

Repeat this procedure for any additional standard partitions you want to create.
