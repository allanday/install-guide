
:experimental:

[[appe-Publican-Revision_History]]
= Revision History

Note that revision numbers relate to the edition of this manual, not to version numbers of Fedora.

`1.5-0`:: Tue Oct 30 2018, Petr Bokoč (pbokoc@redhat.com)

* Fedora 29 Final release

* All external links are now converted to HTTPS where applicable (link:++https://pagure.io/fedora-docs/install-guide/issue/3++[issue 3])

* Removed a mention of a no longer available rescue image (link:++https://pagure.io/fedora-docs/install-guide/issue/4++[issue 4])

* New Kickstart commands and options: `url`, `network --bindto=`, and LUKS-related options for `autopart`, `part`, `logvol`, and `raid`.

* The `authconfig` Kickstart command is now deprecated and replaced by `authselect` (link:++https://pagure.io/fedora-docs/install-guide/issue/6++[issue 6])

* New boot options: `inst.stag2.all=`, `inst.ks.all=`, `inst.xtimeout=`, `inst.decorated=`

* Updated the Fedora Media Writer chapter (link:++https://pagure.io/fedora-docs/install-guide/issue/7++[issue 7])

* Updated image verification instructions for Mac (link:++https://pagure.io/fedora-docs/install-guide/issue/15++[issue 15])

* Various fixes related to the new publishing system

`1.4-0`:: Mon Jun 20 2016, Clayton Spicer (cspicer@redhat.com)

* Fedora 24 Final release

* Added boot option [option]#inst.nosave=#

* Typo fixes

`1.3-0`:: Mon Nov 02 2015, Petr Bokoč (pbokoc@redhat.com)

* Fedora 23 Final release

* Kickstart commands and boot options updated for F23

* Many typo fixes

`1.2-0`:: Mon May 25 2015, Petr Bokoč (pbokoc@redhat.cogm)

* Fedora 22 Final release

* References to [application]*Yum* replaced with [application]*DNF* (the new default package manager)

* Updates in boot options: [option]#inst.dnf# replaced with [option]#inst.nodnf#, added [option]#inst.kdump_addon=#

* Updates in Kickstart: [command]#%anaconda#, [command]#%addon#, [command]#sshkey#, [command]#pwpolicy#

* Added the `Kdump` screen to the GUI docs

`1.1-2`:: Sun Dec 21 2014, Zach Oglesby (zach@oglesby.co)

* Fixed Windows checksum steps, BZ#1175759

`1.1-1`:: Mon Dec 15 2014, Petr Bokoč (pbokoc@redhat.com)

* Fixes in Boot Options: inst.headless is deprecated, inst.askmethod moved from removed to Installation Source

* Parts of the network boot setup chapter have been updated with correct URLs

* Updates in Recommended Partitioning Scheme and Advice on Partitions in the Manual Partitioning section of the installation chapter

`1.1-0`:: Mon Dec 8 2014, Petr Bokoč (pbokoc@redhat.com)

* Publishing for Fedora 21

`1.0-1`:: Mon Dec 8 2014, Pete Travis (immanetize@fedoraproject.org)

* added info on media types

`1.0-0`:: Tue Dec 17 2013, Petr Bokoč (pbokoc@redhat.com)

* Publishing for Fedora 20
